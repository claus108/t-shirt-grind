<?php


    add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
    function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    }

    $product_categories = array("software", "plugins", "merch");

    // disable xmlrpc
function remove_xmlrpc_methods( $methods ) {
    return array();
  }
  add_filter( 'xmlrpc_methods', 'remove_xmlrpc_methods' );

?>